const REGEXP_MATCHER = /^(.)|\s+(.)/g;

/**
 * Uppercase the first character of each word in the given `str`.
 */
export default function ucwords(str: string): string {
  return str.toLocaleLowerCase().replace(REGEXP_MATCHER, ($1) => {
    return $1.toLocaleUpperCase();
  });
}
