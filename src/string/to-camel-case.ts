import ucfirst from "./ucfirst";
import unaccent from "./unaccent";

const REGEXP_CAMEL = /([A-Z]{1,})/g;
const REGEXP_MATCHER = /[A-Z\xC0-\xD6\xD8-\xDE]?[a-z\xDF-\xF6\xF8-\xFF\d]+/g;

/**
 * Returns the given `str` in `camelCased` format.
 */
export default function toCamelCase(str: string, upperFirst = false): string {
  if (str.length === 1) {
    if (upperFirst) {
      return str.charAt(0).toLocaleUpperCase();
    }

    return str;
  }

  // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
  str = unaccent(str)
    .replace(REGEXP_CAMEL, "-$1")
    .toLowerCase()
    .match(REGEXP_MATCHER)!
    .reduce((result: string, word: string): string => {
      return result + ucfirst(word);
    });

  if (upperFirst) {
    return str.charAt(0).toLocaleUpperCase() + str.substr(1);
  }

  return str;
}
