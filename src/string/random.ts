import { randomBytes } from "crypto";
import removePunctuation from "./remove-punctuation";

const ALLOWED_CHARS = "ABCDEFGHJKLMNPRTUVWXYZabcdefghjkmnopqrtuvwxyz346789";

/**
 * Generate "safe" pseudorandom strings.
 *
 * @note allowedChars **MUST** contain alphanumeric chars only
 */
export default function random(length = 24, allowedChars = ALLOWED_CHARS): string {
  let chars = removePunctuation(allowedChars);

  if (!chars) {
    chars = ALLOWED_CHARS;
  }

  const filterFn = new RegExp(`[^${chars}]`, "g");
  let result = "";

  while (result.length < length) {
    result += randomBytes(length).toString("hex").replace(filterFn, "");
  }

  return result.substr(0, length);
}
