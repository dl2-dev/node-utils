type MemoizeFn = (str: string) => string;

/**
 * Super fast memoize for functions.
 */
export default function memoize(fn: MemoizeFn): MemoizeFn {
  const cache = new Map();

  return (input: string): string => {
    let output: string = cache.get(input) as string;

    if (output === undefined) {
      output = fn.apply(null, [input]);
      cache.set(input, output);
    }

    return output;
  };
}
