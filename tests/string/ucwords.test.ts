import ucwords from "../../src/string/ucwords";

describe("string", () => {
  function* dataProvider(): Generator<[string, string]> {
    yield ["Crème Brulée", "Crème Brulée"];
    yield ["CRÈME BRULÉ!", "Crème Brulé!"];
    yield ["Mon café est PLEIN de caféïne!", "Mon Café Est Plein De Caféïne!"];
  }

  test("#ucwords", () => {
    for (const [input, expected] of dataProvider()) {
      const out = ucwords(input);

      expect(out).toEqual(expected);
    }
  });
});
