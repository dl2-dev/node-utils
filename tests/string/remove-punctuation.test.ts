import removePunctuation from "../../src/string/remove-punctuation";

describe("string", () => {
  describe("#removePunctuation", () => {
    function* dataProvider(): Generator<[string, string, string]> {
      yield [
        "Text is! 123 test TText123test !!12 // --!~@#$%%^&*( (test)",
        "Text is 123 test TText123test 12 test",
        "Text is$ 123 test TText123test $$12 $$ $$$$$$$$$$$$$ $test$",
      ];

      yield [
        "crème -brulée! I love it!",
        "crème brulée I love it",
        "crème $brulée$ I love it$",
      ];
    }

    it("should remove any punctuation (default)", () => {
      for (const [input, expected] of dataProvider()) {
        const out = removePunctuation(input);

        expect(out).toEqual(expected);
      }
    });

    it("should replace any punctuation with `$`", () => {
      for (const [input, , expected] of dataProvider()) {
        const out = removePunctuation(input, "$");

        expect(out).toEqual(expected);
      }
    });
  });
});
