import random from "../../src/string/random";
import removePunctuation from "../../src/string/remove-punctuation";

describe("string", () => {
  function* dataProvider(): Generator<[number, string | undefined, string | RegExp]> {
    yield [10, "a", /a/g];
    yield [10, "12", /12/g];
    yield [11, "@#&", "random()"];
    yield [12, "!", "random()"];
    yield [13, undefined, "random()"];
  }

  describe("#random", () => {
    it("should have 24 chars length (default parameters)", () => {
      const out = random();

      expect(out).toHaveLength(24);
    });

    it("should contain only alphanumeric chars", () => {
      for (const [length, chars] of dataProvider()) {
        const out = random(length, chars);

        expect(out).toEqual(removePunctuation(out));
      }
    });

    for (const [length, chars, expected] of dataProvider()) {
      // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
      it(`should have ${length} chars length and match "${expected}" result`, () => {
        const out = random(length, chars);

        expect(out).toHaveLength(length);

        if (expected !== "random()") {
          expect(out).toMatch(expected);
        }
      });
    }
  });
});
