import cpf from "../../src/string/cpf";

describe("string", () => {
  function* dataProvider(): Generator<[string, boolean?]> {
    yield ["000.000.001-91"];
    yield ["000.000.001-92", true];
    yield ["000.000.000-00", true];
    yield ["000.000.000-10", true];
    yield ["120.000.000-00", true];
    yield ["120.000.000-0A", true];
  }

  describe("#cpf", () => {
    it("should throw Error on invalid input", () => {
      for (const [input, invalid] of dataProvider()) {
        const ERRORMSG = new Error(`type '${input}' is not assignable to type 'cpf'`);
        const out: () => string = () => cpf(input);

        if (invalid) {
          expect(out).toThrow(ERRORMSG);
        }
      }
    });

    it("should format the output", () => {
      for (const [input, invalid] of dataProvider()) {
        if (invalid) {
          continue;
        }

        const out = cpf(input, true);

        expect(out).toMatch(/\d{3}.\d{3}.\d{3}-\d{2}/);
      }
    });

    it("should not format the output", () => {
      for (const [input, invalid] of dataProvider()) {
        if (invalid) {
          continue;
        }

        const out = cpf(input);

        expect(out).toHaveLength(11);
        expect(out).toMatch(/\d+/);
      }
    });
  });
});
