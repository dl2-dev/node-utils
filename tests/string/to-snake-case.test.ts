import toSnakeCase from "../../src/string/to-snake-case";

describe("string", () => {
  function* dataProvider(): Generator<string> {
    yield "__FOO-BAR__   !";
    yield "--foo-bár--";
    yield "--foo-bar!";
    yield "Foo Bar";
    yield "Foo______BAR";
    yield "fooBar";
    yield "fooBAR";
    yield "FOoBar";
  }

  describe("#toSnakeCase", () => {
    it("should use `_` as separator (default)", () => {
      for (const input of dataProvider()) {
        const out = toSnakeCase(input);

        expect(out).toEqual("foo_bar");
      }
    });

    it("should use `+` as separator", () => {
      for (const input of dataProvider()) {
        const out = toSnakeCase(input, "+");

        expect(out).toEqual("foo+bar");
      }
    });
  });
});
