import toCamelCase from "../../src/string/to-camel-case";

describe("string", () => {
  function* dataProvider(): Generator<string> {
    yield "__FOO-BAR__   !";
    yield "--foo-bár--";
    yield "--foo-bar!";
    yield "Foo Bar";
    yield "Foo______BAR";
    yield "fooBar";
    yield "fooBAR";
    yield "FOoBar";
  }

  describe("#toCamelCase", () => {
    it("should return the first character in lowercase format when `upperFirst` is false (default)", () => {
      for (const input of dataProvider()) {
        const out = toCamelCase(input);

        expect(out).toEqual("fooBar");
      }
    });

    it("should return the first character in uppercase format when `upperFirst` is true", () => {
      for (const input of dataProvider()) {
        const out = toCamelCase(input, true);

        expect(out).toEqual("FooBar");
      }
    });

    it("should return the given string as is if it have only 1 char (e.g.: don't remove punctuation or special chars)", () => {
      expect("*").toEqual(toCamelCase("*"));
      expect("!").toEqual(toCamelCase("!"));
      expect("a").toEqual(toCamelCase("a"));
      expect("A").toEqual(toCamelCase("a", true));
    });
  });
});
