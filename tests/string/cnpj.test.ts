import cnpj from "../../src/string/cnpj";

describe("string", () => {
  function* dataProvider(): Generator<[string, boolean?]> {
    yield ["00.000.000/0001-91"];
    yield ["00.000.000/0001-92", true];
    yield ["00.000.000/0000-00", true];
    yield ["00.000.000/0000-10", true];
    yield ["12.000.000/0000-00", true];
    yield ["12.000.000/0000-0A", true];
  }

  describe("#cnpj", () => {
    it("should throw Error on invalid input", () => {
      for (const [input, invalid] of dataProvider()) {
        const ERRORMSG = new Error(`type '${input}' is not assignable to type 'cnpj'`);
        const out: () => string = () => cnpj(input);

        if (invalid) {
          expect(out).toThrow(ERRORMSG);
        }
      }
    });

    it("should format the output", () => {
      for (const [input, invalid] of dataProvider()) {
        if (invalid) {
          continue;
        }

        const out = cnpj(input, true);

        expect(out).toMatch(/\d{2}.\d{3}.\d{3}\/\d{4}-\d{2}/);
      }
    });

    it("should not format the output", () => {
      for (const [input, invalid] of dataProvider()) {
        if (invalid) {
          continue;
        }

        const out = cnpj(input);

        expect(out).toHaveLength(14);
        expect(out).toMatch(/\d+/);
      }
    });
  });
});
