import ucfirst from "../../src/string/ucfirst";

describe("string", () => {
  function* dataProvider(): Generator<[string, string]> {
    yield ["Crème Brulée", "Crème brulée"];
    yield ["CRÈME BRULÉ!", "Crème brulé!"];
  }

  test("#ucfirst", () => {
    for (const [input, expected] of dataProvider()) {
      const out = ucfirst(input);

      expect(out).toEqual(expected);
    }
  });
});
