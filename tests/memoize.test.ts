import memoize from "../src/memoize";

test("memoize", () => {
  let calls = 0;
  const memoized = memoize((str) => {
    ++calls;

    return str;
  });

  memoized("created_at");
  memoized("created_at");
  memoized("updated_at");
  memoized("updated_at");

  expect(calls).toBe(2);
});
